module Strass where

import Numeric.LinearAlgebra.HMatrix 
import TeraReg.QuadTree
import TeraReg.MatrixMult
-- import TeraReg.DataGen

quadFromMatrix :: Matrix Double -> Quad (Matrix Double)
quadFromMatrix x =
    quadRect x q
    where
        (h, w) = size x
        q = fromRect $ Rect 0 0 w h

-- m = matrix 2 [ -0.25004703455550054,
--                -0.33362919666425706 , 0.9321802196981601, -0.11328705513933457 ,
--                0.1756426309991656, -0.528605617773121 ]

-- m :: Matrix Double
-- m = matrix 2 [
--     -0.25004703455550054, -0.33362919666425706, 0.7676085589567515,
--     -0.3039332177930579, 0.14547049386223757, -0.5399529279749015 ,
--     -0.4570435261240128, -1.1159671217587463, -1.0948219629308655,
--     0.3859040712095898, -0.7466217781034123, 2.4707995338821342e-2 ]

mult :: Matrix Double -> IO ()
mult m = do
    -- let m = generatePredictors 8675309 16 4 0.25
    let n = tr m
    let p0 = m `mul` n
    let p1 = m `strassen` n
    let p2 = qtStrass m
    
    -- let mq = quadFromMatrix m
    -- let nq = quadFromMatrix n
        
    print "m"
    print m
    print ""

    print "n"
    print n
    print ""

    print "p0"
    print p0
    print ""

    print "p1"
    print p1
    print ""

    print "p1 - p0"
    print $ maxElement $ abs $ p1 - p0
    print ""

    print "p2"
    print p2
    print ""

    print "p2 - p0"
    print $ maxElement $ abs $ p2 - p0
    print ""


