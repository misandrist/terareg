{-# Language FlexibleInstances #-}

module Tests (tests) where

import Distribution.TestSuite.QuickCheck
import Test.QuickCheck
import Numeric.LinearAlgebra.HMatrix 
import TeraReg.QuadTree
import TeraReg.MatrixMult
import TeraReg.DataGen
import TeraReg.Strassen

tests :: IO [Test]
tests = return [
    testProperty "Trans Rect" check_transRect,
    testProperty "Strassen" check_strassen,
    testProperty "Strassen Quads" check_strassQuads,
    testProperty "Strassen QuadTree" check_qtStrass

    ]

epsilon :: Double
epsilon = 0.00001

seed :: Gen Seed
seed = choose (-1000000000, 10000000000)

instance Arbitrary (Matrix Double) where
    arbitrary = sized $ \n -> do
        s <- seed
        return $ generatePredictors s ((n + 1) * 2) ((n + 1) * 8) 0.25

instance Arbitrary (Rect) where
    arbitrary = sized $ \n -> return $ Rect 0 0 (n*2) (n*4)
    
quadFromMatrix :: Matrix Double -> Quad (Matrix Double)
quadFromMatrix m =
    fmapSubMatrixRect m q
    where
        (h, w) = size m
        q = fromRect $ Rect 0 0 w h

check_strassen :: Matrix Double -> Bool
check_strassen m =
    let
        n = tr m
        p0 = m `mul` n
        p1 = m `strassen` n
        adm = abs (p1 - p0)
    in
        maxElement adm <= epsilon
        
check_strassQuads :: Matrix Double -> Bool
check_strassQuads m =
    let
        n = tr m
        qm = quadFromMatrix m
        qn = quadFromMatrix n
        p0 = m `mul` n
        p1 = qm `strassQuads` qn
        adm = abs (p1 - p0)
    in
        maxElement adm <= epsilon

check_qtStrass :: Matrix Double -> Bool
check_qtStrass m =
    let
        n = tr m
        p0 = m `mul` n
        p1 = m `qtStrass` n
        adm = abs (p1 - p0)
    in
        maxElement adm <= epsilon

check_transRect :: Rect -> Bool
check_transRect r@(Rect l t h w) =
    let r' = Rect t l w h in
        r' == transRect r
