module TeraReg.LinReg where

import Numeric.LinearAlgebra.HMatrix
import TeraReg.MatrixMult (strassen, strassen')
import TeraReg.Strassen (qtStrass)

ols :: 
    (Field t, Numeric t) =>
    Matrix t -> Vector t -> Vector t
ols x y = (pinv ((tr x) `mul` x )) #> ((tr x) #> y)

iols :: 
    Numeric t =>
    Vector t -> Vector t -> t
iols x b = x <·> b

fast_ols :: Matrix Double -> Vector Double -> Vector Double
fast_ols x y =
    let i0 = strassen (tr x) x in
    (pinv i0) #> ((tr x) #> y)

fast_ols_io :: Matrix Double -> Vector Double -> IO (Vector Double)
fast_ols_io x y = do
    i0 <- strassen' (tr x) x
    return $ (pinv i0) #> ((tr x) #> y)

qtstrass_ols :: Matrix Double -> Vector Double -> Vector Double
qtstrass_ols x y =
    let i0 = qtStrass (tr x) x in
    (pinv i0) #> ((tr x) #> y)
